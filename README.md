A brief explanation of source codes, any questions regarding to codes, please reach me directly.

# Projects:

## CommandServer:
A tcp server based on Windows IOCP for testing purpose, it simply accepts command from tcp client and response with result, once a new connect comes, it sends a welcome message to client “Hello,xxxx”,
2 commands are supported, DATE and QUIT, for DATE, it responses current datetime string, for QUIT, it closes TCP connection, each command should be ended with \r\n
telnet client is supported

## tcp-clients-cpp:
Simpleclient.cpp——A simple tcp client, it connects to command server above, receives hello message from server and sends DATE command then receives date time string from server per second, it uses blocking mode socket.
it supports to be built to native binary with gcc or wasm by emcc, for wasm, only modularize=0 works, modularize=1 could have compile fine, but not able to run.

multiplexing.cpp——A non-blocking tcp client, it connects to command server with non-blocking tcp socket with select(), receives hello message from server and sends DATE command then receives date time string from server per second
it supports to be built to native binary with gcc or wasm by emcc, for wasm, only modularize=0 works, modularize=1 could have compile fine, but  not able to run.

Native binaries can talk to commandserver.
All wasms cannot talk to CommandServer directly, as wasm talks websocket, commandserver talks raw TCP, needs websockify for proxying.

## tcpclient-chrome-extension:
it implemented a ws client using browser's websocket infrastructures, also it demonstrates how to load the wasm that convereted from a tcp client using raw tcp socket, pass arguments to it and makes it work within web worker.

## ws-server-python:
A simple websocket server for test purpose, it just returns the message followed by “Your said:”
requires python3, and pip3 tornado==6.0.3, tornado-websocket-server==0.1.5a1
