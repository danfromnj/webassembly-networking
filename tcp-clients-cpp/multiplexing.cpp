#include <sys/socket.h>
#include <netdb.h>
#include <memory.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <iostream>
#include <sys/time.h>
#include <sys/select.h>

#ifdef __EMSCRIPTEN__
#include <emscripten.h>
#endif

template<typename T>
void one_iter(T sockfd) 
{
    std::cout << "Enter one_iter " << std::endl;
    fd_set rfds;
    FD_ZERO(&rfds);
    FD_SET((int)sockfd,&rfds);
    struct timeval tv = {2,0};
    int ret = select((int)sockfd + 1, &rfds, 0, NULL, &tv);
    if(ret == 1)
    {
        std::cout << "Data is ready to read" << std::endl;
        char recvBuff[1024] = {0};
        const int n = recv((int)sockfd, recvBuff, sizeof(recvBuff)-1, 0);
        if( n >= 0)
        {
            std::cout << "received " << n << " bytes: " << recvBuff << std::endl;
            const std::string command = "DATE\r\n";
            int nSend = send((int)sockfd, command.c_str(), command.length(), 0);
        }
        else
        {
            std::cout << "recv failed, errno = " << errno << std::endl;
        }    
    }
}

extern "C"
int MakeConnection(const char* cszServer, int port)
{
    std::cout << "Creating TCP connection to " << cszServer << ":" << port << std::endl;
    int sockfd = 0; 
    if((sockfd = socket(AF_INET, SOCK_STREAM | SOCK_NONBLOCK, 0)) < 0)
    {
        std::cout << std::endl << "Error : Could not create socket" << std::endl;
        return 1;
    }

    struct sockaddr_in serv_addr = {0};
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(port);

    const hostent* ht = gethostbyname(cszServer);
    if(!ht)
    {
        std::cout << "gethostname failed for " << cszServer << std::endl;
        return 1;
    }

    memcpy(&(serv_addr.sin_addr.s_addr), ht->h_addr, ht->h_length);

    if(connect(sockfd, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0)
    {
        if(errno == EINPROGRESS)
        {
            fd_set wfds;
            FD_ZERO(&wfds);
            FD_SET(sockfd,&wfds);
            struct timeval tv = {5,0};
            int ret = select((int)(sockfd + 1), NULL, &wfds, NULL, &tv);
            if(ret == 1)
            {
                std::cout << "Connection is established, ready to send data to the server." << std::endl;
#ifdef __EMSCRIPTEN__
                emscripten_set_main_loop_arg(one_iter<void*>, (void*) sockfd, 1, true);
#else
                while(true)
                {
                    one_iter<int>(sockfd);
                    sleep(1);
                }
#endif
            }
        }
        else
        {
            std::cout << "connect failed!." << std::endl;
        }
    }

    close(sockfd);
    return 0;
}

// # Compile for a native program with gcc:
// g++ multiplexing.cpp -o multiplexing
// # Compile to run in node:
// emcc multiplexing.cpp -s WASM=1 -o multiplexing.js
// # Compile with modularized wasm:
// emcc multiplexing.cpp -s WASM=1 -s MODULARIZE=1  -s EXPORTED_FUNCTIONS='["_MakeConnection"]' -s EXTRA_EXPORTED_RUNTIME_METHODS='["cwrap", "setValue"]' -o multiplexing.js
int main(int argc, char *argv[])
{
    if(argc != 3)
    {
        std::cout << std::endl << "Usage: " << argv[0] << " ip port" << std::endl;
        return 1; 
    }

    return MakeConnection(argv[1], std::stoi(argv[2]));
}