#include <sys/socket.h>
#include <netdb.h>
#include <memory.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <iostream>

#ifdef __EMSCRIPTEN__
#include <emscripten.h>
#endif


template<typename T>
void one_iter(T sockfd) 
{
    std::cout << "Entered one_iter " << std::endl;
    char recvBuff[1024] = {0};
    int n =  recv((int)sockfd, recvBuff, sizeof(recvBuff)-1, 0);
    if(n >= 0)
    {
        std::cout << n << " bytes received: " << recvBuff << std::endl;
        const std::string command = "DATE\r\n";
        send((int)sockfd, command.c_str(), command.length(), 0);
    }
    else
    {
        std::cout << "n = " << n << " receive failed, errno = " << errno << std::endl;
    }
}

// compile to native:
// g++ simpleclient.cpp -o simpleclient
// compile to wasm:
// emcc simpleclient.cpp -s WASM=1 -o simpleclient.js
int main(int argc, char *argv[])
{
    if(argc != 3)
    {
        std::cout << std::endl << "Usage: " << argv[0] << " ip port" << std::endl;
        return 1; 
    }

    int sockfd = 0; 
    if((sockfd = socket(AF_INET, SOCK_STREAM, 0)) < 0)
    {
        std::cout << std::endl << "Error: failed to create socket." << std::endl;
        return 1;
    }

    struct sockaddr_in serv_addr = {0};
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(std::stoi(argv[2]));

    const hostent* ht = gethostbyname(argv[1]);
    if(!ht)
    {
        std::cout << "gethostname failed for " << argv[1] << std::endl;
        return 1;
    }

    memcpy(&(serv_addr.sin_addr.s_addr), ht->h_addr, ht->h_length);

    if(connect(sockfd, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0)
    {
#ifdef __EMSCRIPTEN__
        // just ignore EINPROGRESS in the context of websocket in WASM/js 
        if(errno != EINPROGRESS)
        {
            std::cout << "connect failed, errno = " << errno << std::endl;
            return 1;
        }
#else
        std::cout << "connect failed, errno = " << errno << std::endl;
        return 1;
#endif
    }

    std::cout << std::endl << "connect succeeded." << std::endl;

#ifdef __EMSCRIPTEN__
    emscripten_set_main_loop_arg(one_iter<void*>, (void*) sockfd, 1, true);
#else
    while(true)
    {
        one_iter<int>(sockfd);
        sleep(1);
    }
#endif
    return 0;
}