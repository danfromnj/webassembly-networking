#include "AysnServer.h"
#include <process.h>
#include <functional>
#include <algorithm>

CSessionManager* g_pSessionMgr = NULL;

CAysnServer::CAysnServer(unsigned short usDefPort,unsigned long uMaxCocurrentThread):
m_usDefPort(usDefPort),
m_hListenThread(NULL),
m_hIocp(CreateIoCompletionPort(INVALID_HANDLE_VALUE, NULL, NULL,uMaxCocurrentThread)),
m_hShutDownEvent(CreateEvent(NULL,FALSE,FALSE,NULL)),
m_uMaxCocurrentThread(uMaxCocurrentThread)
{
	if(uMaxCocurrentThread == 0)
	{
		SYSTEM_INFO sysInfo = {0};
		::GetSystemInfo(&sysInfo);
		uMaxCocurrentThread = sysInfo.dwNumberOfProcessors;
	}

	g_pSessionMgr = &m_sessionMgr;

	WSADATA wsaData;
	if (WSAStartup(MAKEWORD(2, 2), &wsaData))
	{
		throw std::exception("Init socket failed!");
	}
}

CAysnServer::~CAysnServer()
{
	WSACleanup();
}

unsigned __stdcall CAysnServer::SvrListenThread(void* param)
{
	CAysnServer* pServer = reinterpret_cast<CAysnServer*>(param);
	return pServer->DoListen();
}

unsigned CAysnServer::DoListen()
{
	struct sockaddr_in addr;
	addr.sin_family = AF_INET;
	addr.sin_port = htons(this->m_usDefPort);
	addr.sin_addr.S_un.S_addr = htonl(INADDR_ANY);

	SOCKET_HANDLE hSock = ::WSASocket(addr.sin_family, SOCK_STREAM, IPPROTO_TCP, NULL, 0, WSA_FLAG_OVERLAPPED);
	if(hSock == INVALID_SOCKET)
	{
		return -1;
	}

	if(::bind(hSock,(const sockaddr *)&addr,sizeof(addr)) != SOCKET_ERROR)
	{
		if(::listen(hSock,64) != SOCKET_ERROR)
		{
			while(true)
			{
				TryServeNewConnection(hSock);
				if(IsShutDown()) break;
			}
		}
		else
		{
			return -1;
		}
	}
	return 0;
};

void CAysnServer::TryServeNewConnection(SOCKET hSock)
{
	fd_set acceptSet;
	FD_ZERO(&acceptSet);
	FD_SET(hSock,&acceptSet);

	timeval to = {4,0};
	select(0,&acceptSet,NULL,NULL,&to);
	if(FD_ISSET(hSock,&acceptSet))
	{
		sockaddr_in addr;
		int len = sizeof(addr);
		SOCKET hAccept = ::accept(hSock,(sockaddr*)&addr,&len);

		CGenericSession* pSession = m_sessionMgr.CreateSession(hAccept);

		if(!pSession)
		{
			//Error handle, send error message to the client
		}

		//Bind the new socket to IOCP
		CreateIoCompletionPort((HANDLE)hAccept,m_hIocp,(ULONG_PTR)pSession,0);
		this->KickOffConnectEvent(pSession);
	}
}

bool CAysnServer::IsShutDown()
{
	return WaitForSingleObject(m_hShutDownEvent,0) == WAIT_OBJECT_0;
}

void CAysnServer::StartServer()
{
	unsigned uThreadID = 0;
	for(unsigned long i = 0; i < m_uMaxCocurrentThread; ++i)
	{
		m_vecWorkingThreads.push_back((HANDLE)::_beginthreadex(NULL,0,CAysnServer::SvrWorkingThread,this,0,&uThreadID));
	}

	m_hListenThread = (HANDLE)::_beginthreadex(NULL,0,CAysnServer::SvrListenThread,this,0,&uThreadID);
}

void CAysnServer::StopServer()
{
	::SetEvent(m_hShutDownEvent);
	for(size_t i = 0;i < m_vecWorkingThreads.size(); ++i)
	{
		PostQueuedCompletionStatus(m_hIocp,SYSTEM_SHUTDOWN_LEN,NULL,NULL);
		WaitForSingleObject(m_hListenThread,INFINITE);
		WaitForMultipleObjects(m_vecWorkingThreads.size(), &m_vecWorkingThreads[0], true, INFINITE);
		CloseHandle(m_hListenThread);
		std::for_each(m_vecWorkingThreads.begin(),m_vecWorkingThreads.end(),std::ptr_fun(::CloseHandle));
	}
}


unsigned long CAysnServer::KickOffConnectEvent(CGenericSession* pSession)
{
	PostQueuedCompletionStatus(m_hIocp,SESSION_CONNECT_LEN,(ULONG_PTR)pSession,&(pSession->RefSessionOverlapped()));
	return AS_SUCCESS;
}

unsigned __stdcall CAysnServer::SvrWorkingThread(void* param)
{
	CAysnServer* pServer = reinterpret_cast<CAysnServer*>(param);
	return pServer->DoWork();
}
unsigned CAysnServer::DoWork()
{
	DWORD dwBytesTransferred = 0;
	CGenericSession* pSession = NULL;
	SESSION_OVERLAPPED* pOverlapped = NULL;
	while(true)
	{
		BOOL bRet = GetQueuedCompletionStatus(m_hIocp,&dwBytesTransferred,(PULONG_PTR)&pSession,(LPOVERLAPPED*)&pOverlapped,INFINITE);
		if(!bRet)
		{
			//Error handle depends on GetLastError() and whether pOverlapped is NULL or not
			if(!pOverlapped)
			{
				/*
				If *lpOverlapped is NULL and the function does not dequeue 
				a completion packet from the completion port, 
				the return value is zero. 

				The function does not store information in the variables 
				pointed to by the lpNumberOfBytes and lpCompletionKey parameters. 

				To get extended error information, call GetLastError. 
				If the function did not dequeue a completion packet 
				because the wait timed out, GetLastError returns WAIT_TIMEOUT.
				*/
			}
			else
			{
				/*
				If *lpOverlapped is not NULL and the function dequeues 
				a completion packet for a failed I/O operation 
				from the completion port, the return value is zero. 

				The function stores information in the variables 
				pointed to by lpNumberOfBytes, lpCompletionKey, and lpOverlapped. 

				To get extended error information, call GetLastError.
				*/
				const DWORD dwErr = GetLastError();
				switch(dwErr)
				{
				case ERROR_OPERATION_ABORTED:
				case ERROR_NETNAME_DELETED:
					/*
					When closesocket successfully canceled IO,
					GetQueuedCompletionStatus will return zero,
					and the last error ERROR_OPERATION_ABORTED (995) or
					ERROR_NETNAME_DELETED (64).

					This case should be a result of successful IO cancellation.
					*/
					break;
				default:
					/*
					This execution path is rare. Only when closesocket failed
					to cancel pending IO operation, will execute to this
					path. In this case, we still call to
					pSmtpSession->HandleSession, and handled in the function.
					*/
					break;
				}
			}

		}
		else
		{
			/*
			If the function dequeues a completion packet 
			for a successful I/O operation from the completion port, 
			the return value is nonzero. 

			The function stores information in the variables pointed to 
			by the lpNumberOfBytes, lpCompletionKey, and lpOverlapped parameters.
			*/
		}


		if(pSession)
		{
			pSession->HandleSession(dwBytesTransferred);
		}
		else
		{
			if(dwBytesTransferred == SYSTEM_SHUTDOWN_LEN)
			{
				break;
			}
			else
			{
				//As the pSession is null, but it's not shutdown event, log error
			}

		}
	}
	return 0;
}

unsigned __stdcall CAysnServer::SessionTimeoutCheckThread(void* param)
{
	CAysnServer* pServer = static_cast<CAysnServer*>(param);
	return pServer->DoTimeoutCheck();
}
unsigned CAysnServer::DoTimeoutCheck()
{
	while(true)
	{
		if(WaitForSingleObject(m_hShutDownEvent,1000) == WAIT_OBJECT_0) break;


	}
	return 0;
}