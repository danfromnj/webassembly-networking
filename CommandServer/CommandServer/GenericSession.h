#pragma once
#include "ConfigDefineMacros.h"
#include <WinSock2.h>
enum class IO_EVENT
{
	IO_CONNECT,
	IO_CLOSE, 
	IO_RECV, 
	IO_SEND, 
	IO_TIMEOUT
};


struct SESSION_OVERLAPPED : public WSAOVERLAPPED
{
	IO_EVENT   ioevent;
};

class CGenericSession
{
public:
	CGenericSession();
	virtual ~CGenericSession();
public:
	virtual unsigned long HandleSession(unsigned long ulBytesTransferred) = 0;
	SESSION_OVERLAPPED& RefSessionOverlapped(){return m_overlapped;}

	DEFINE_PROPERTY_BOOLEAN(Dead);

protected:
	CRITICAL_SECTION m_criticalSection;
	SESSION_OVERLAPPED m_overlapped;
};