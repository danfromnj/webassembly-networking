// CommandServer.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include "AysnServer.h"
#include <string>
int wmain(int argc, wchar_t** argv)
{
	unsigned short port = 17904;
	if (argc >= 2) {
		port = std::stoi(argv[1]);
	}
	CAysnServer server(port, 1);
	server.StartServer();
	std::cout << "Now listening on port " << port << std::endl;
	system("pause");
	server.StopServer();
	return 0;
}