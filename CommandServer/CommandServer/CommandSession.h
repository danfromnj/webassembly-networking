#pragma once
#include "GenericSession.h"
#include "SessionManager.h"
#include <string>
enum class IO_OPERATION
{
	IO_OPERATION_NONE = 0,

	//
	// Don't need to issue another IO operation. Should process pending data.
	//
	IO_OPERATION_CONTINUE_PROCESS,

	//
	// Should issue another Receive operation.
	//
	IO_OPERATION_RECV_FROM_SOCKET,

	//
	// Should issue shutdown operation.
	//
	IO_OPERATION_CLOSE_CONENCTION,

	__IO_OPERATION_LAST
};

enum
{
	RECV_BUF_LEN = 4096,
};

enum class CMD_EXEC_RET_CODE
{
	CMD_SUCCESS,
	CMD_FAIL,
	__CMD_EXEC_RET_CODE_LAST
};

class CCommandSession : CGenericSession
{
	friend class CSessionManager;
public:
	virtual unsigned long HandleSession(unsigned long ulBytesTransferred);
private:
	CCommandSession(SOCKET hSock,CSessionManager& sessionMgr);
	~CCommandSession();
	bool HandleIoComplete(unsigned long ulBytesTransferred,IO_EVENT ioevent);

	IO_OPERATION ProcessConnectEvent();
	IO_OPERATION ProcessReceivedData(unsigned long ulBytesTransferred);
	IO_OPERATION ProcessCommand();
	virtual CMD_EXEC_RET_CODE HandleCommand(const char* cszCommand);

	CMD_EXEC_RET_CODE HandleCMDDate(const char* cszCommand);
	CMD_EXEC_RET_CODE HandleCMDQuit(const char* cszCommand);

	void Shutdown();
	unsigned long IssueAysnRead();
private:
	SOCKET m_hSock;
	CSessionManager& m_sessionMgr;

	char m_recvBuf[RECV_BUF_LEN] = { 0 };
	std::string m_dataBuffer;
};
