#include "SessionManager.h"
#include "CommandSession.h"
#include  <algorithm>

class CCriticalSectionLock
{
public:
	CCriticalSectionLock(CRITICAL_SECTION* pCriticalSection) :m_pCriticalSection(pCriticalSection)
	{
		EnterCriticalSection(m_pCriticalSection);
	}
	~CCriticalSectionLock()
	{
		LeaveCriticalSection(m_pCriticalSection);
	}
private:
	CRITICAL_SECTION* m_pCriticalSection;
};



CSessionManager::CSessionManager(size_t sMaxSession): m_sMaxSession(sMaxSession)
{
	InitializeCriticalSection(&m_criticalSection);
}

CSessionManager::~CSessionManager()
{
	DeleteCriticalSection(&m_criticalSection);
}

CGenericSession* CSessionManager::CreateSession(SOCKET hSock)
{
	if(this->GetSessionCount() >= m_sMaxSession) 
	{
		return NULL;
	}
	CCommandSession* pSession = new CCommandSession(hSock,*this);
	this->AddSession(pSession);
	return pSession;
}

size_t CSessionManager::DestroySession(CGenericSession* pSession)
{
	size_t newSize = this->RemoveSession(pSession);
	delete pSession;
	return newSize;
}

void CSessionManager::DestroyDeadSessions()
{
	std::list<CGenericSession*> deadSessions;

	this->m_sessionList.remove_if([&deadSessions](CGenericSession* pSession)->bool
		{
			if (pSession->IsDead())
			{
				deadSessions.push_back(pSession);
				return true;
			}
			return false;
		});

	std::for_each(deadSessions.begin(), deadSessions.end(), [](CGenericSession* pSession) {delete pSession; });
}



size_t CSessionManager::AddSession(CGenericSession* pSession)
{
	CCriticalSectionLock lock(&m_criticalSection);

	if (find(m_sessionList.begin(), m_sessionList.end(), pSession) != m_sessionList.end())
	{
		throw std::exception("The session has been added into the session list, system may meet some fatal error!");
	}
	m_sessionList.push_back(pSession);
	return m_sessionList.size();
}

size_t CSessionManager::RemoveSession(CGenericSession* pSession)
{
	CCriticalSectionLock lock(&m_criticalSection);
	m_sessionList.remove(pSession);
	return m_sessionList.size();
}

size_t CSessionManager::GetSessionCount()const
{
	CCriticalSectionLock lock(&m_criticalSection);
	return m_sessionList.size();
}