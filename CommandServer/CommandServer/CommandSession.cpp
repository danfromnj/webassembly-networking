#include <time.h>
#include "CommandSession.h"
#include "AysnServer.h"

extern CSessionManager* g_pSessionMgr;

namespace 
{
	unsigned long SendBuffer(SOCKET hSock,const char *pcBuffer, size_t nLen)
	{
		if (pcBuffer)
		{
			if(SOCKET_ERROR == send(hSock, pcBuffer, (int)nLen, 0))
			{
				return AS_ERROR_UNKNOWN;
			}
		}

		return AS_SUCCESS;
	}

	unsigned long SendString(SOCKET hSock,const char* pszStr)
	{
		size_t newLen = strlen(pszStr) + 3;
		char *pszSendBuffer = new char[newLen];

		_snprintf_s(pszSendBuffer, newLen-1, _TRUNCATE,"%s\r\n", pszStr);
		pszSendBuffer[newLen-1] = 0;

		size_t iSendLength = strlen(pszSendBuffer);
		return SendBuffer(hSock,pszSendBuffer,iSendLength);
	}

	enum COMMAND_ID
	{
		UNKNOWN = 0, 
		DATE,
		QUIT,
		MAX_CMD,
	};

	//supported commands
#define UNKNOWN_CMD_STR "Unknown"
#define DATE_CMD_STR "DATE"
#define QUIT_CMD_STR "QUIT"
}



CCommandSession::CCommandSession(SOCKET hSock,CSessionManager& sessionMgr):
m_hSock(hSock),
m_sessionMgr(sessionMgr)
{
}

CCommandSession::~CCommandSession()
{
	shutdown(m_hSock,SD_SEND);
	closesocket(m_hSock);
	m_hSock = NULL;
}

unsigned long CCommandSession::HandleSession(unsigned long ulBytesTransferred)
{
	bool bIssueAnotherRead = false;

	//CCriticalSectionLock lock(&this->m_criticalSection);
	if(ulBytesTransferred == 0)//Remote closed socket
	{
		this->RefDead() = true;
		g_pSessionMgr->DestroySession(this);
		goto CHECK;
	}

	if(SESSION_TIME_OUT_LEN == ulBytesTransferred)
	{
		this->Shutdown();
		closesocket(m_hSock);
		m_hSock = INVALID_SOCKET;
		goto CHECK;
	}

	if(IsDead())
	{
		/*
		IO completion after the session marked as dead may happen
		when cancel pending IO operation due to timeout. During
		handling for SESSION_TIME_OUT_LEN event, the session was
		marked as dead, and pending IO operations are canceled. IOCP
		will deque a failure IO package and call into this function 
		again, which will run into this path.
		*/
		goto CHECK;
	}

	bIssueAnotherRead = this->HandleIoComplete(ulBytesTransferred,this->m_overlapped.ioevent);

CHECK:
	if(bIssueAnotherRead)
	{
		unsigned long ret = this->IssueAysnRead();
		if(ret != AS_SUCCESS)
		{
			RefDead() = true;
		}
	}
	return 0;
}

unsigned long CCommandSession::IssueAysnRead()
{
	WSABUF wsaBuf = {sizeof(m_recvBuf) - 1,m_recvBuf};
	::ZeroMemory(&m_overlapped,sizeof(m_overlapped));
	m_overlapped.ioevent = IO_EVENT::IO_RECV;
	DWORD dwFlag = 0;
	DWORD dwBytesTransferred = 0;

	if(SOCKET_ERROR == WSARecv(m_hSock,&wsaBuf,1,&dwBytesTransferred,
		&dwFlag,(LPWSAOVERLAPPED)&m_overlapped,NULL))
	{
		int err = WSAGetLastError();
		switch(err)
		{
		case WSA_IO_PENDING:
			{
				//
				// asyn read operation has been initialized
				//
			}break;
		
		case WSAEWOULDBLOCK:
			{
				//Fatal error
			}break;
		default:
			{
				//
				// remote side has already closed the socket
				//
				return AS_ERROR_UNKNOWN;
			}
		}
	}

	return AS_SUCCESS;

}

IO_OPERATION CCommandSession::ProcessConnectEvent()
{
	//Do IP Filtering here.
	SendString(m_hSock,"Hello,I am an server uses IOCP.\r\n");
	return IO_OPERATION::IO_OPERATION_RECV_FROM_SOCKET;
}

IO_OPERATION CCommandSession::ProcessReceivedData(unsigned long ulBytesTransferred)
{
	m_dataBuffer.append(m_recvBuf,ulBytesTransferred);
	return ProcessCommand();
}

IO_OPERATION CCommandSession::ProcessCommand()
{
	while(true)
	{
		size_t indexOfCRLF = m_dataBuffer.find("\r\n");
		if(indexOfCRLF == std::string::npos)
		{
			//Not enough data to process command
			return IO_OPERATION::IO_OPERATION_RECV_FROM_SOCKET;//Require another read
		}
		std::string command = m_dataBuffer.substr(0, indexOfCRLF + 2);
		m_dataBuffer.erase(0, indexOfCRLF + 2);
		CMD_EXEC_RET_CODE ret = HandleCommand(command.c_str());
		if(ret == CMD_EXEC_RET_CODE::CMD_SUCCESS)
		{
			//Success
		}
		else
		{
			//Error handle
		}

	}

}

CMD_EXEC_RET_CODE CCommandSession::HandleCMDDate(const char* cszCommand)
{
	time_t ltime;
	time(&ltime);
	char timeBuf[64] = {0};
	ctime_s(timeBuf,_countof(timeBuf),&ltime);
	SendString(m_hSock,timeBuf);
	return CMD_EXEC_RET_CODE::CMD_SUCCESS;
}

CMD_EXEC_RET_CODE CCommandSession::HandleCMDQuit(const char* cszCommand)
{
	SendString(m_hSock,"Service closing transmission channel.\r\n");
	Shutdown();
	return CMD_EXEC_RET_CODE::CMD_SUCCESS;
}

CMD_EXEC_RET_CODE CCommandSession::HandleCommand(const char* cszCommand)
{
	static const char* cszCmdStr[MAX_CMD] = 
	{
		UNKNOWN_CMD_STR,
		DATE_CMD_STR,
		QUIT_CMD_STR
	};

	CMD_EXEC_RET_CODE ret = CMD_EXEC_RET_CODE::CMD_SUCCESS;

	if(!_strnicmp(cszCommand,DATE_CMD_STR,sizeof(DATE_CMD_STR) - 1) && isspace((unsigned char)cszCommand[sizeof(DATE_CMD_STR)-1]))
	{
		ret = HandleCMDDate(cszCommand);
	}
	else if(!_strnicmp(cszCommand,QUIT_CMD_STR,sizeof(QUIT_CMD_STR) - 1) && isspace((unsigned char)cszCommand[sizeof(QUIT_CMD_STR)-1]))
	{
		ret = HandleCMDQuit(cszCommand);
	}
	else
	{
		SendString(m_hSock,"Syntax error, command unrecognized.\r\n");
		ret = CMD_EXEC_RET_CODE::CMD_FAIL;
	}

	return ret;
}

bool CCommandSession::HandleIoComplete(unsigned long ulBytesTransferred,IO_EVENT ioevent)
{
	IO_OPERATION nextIo = IO_OPERATION::IO_OPERATION_NONE;

	switch (ioevent)
	{
	case IO_EVENT::IO_CONNECT:
	{
		nextIo = ProcessConnectEvent();
	}break;
	case IO_EVENT::IO_RECV:
	{
		nextIo = ProcessReceivedData(ulBytesTransferred);
	}break;
	case IO_EVENT::IO_SEND:
	{

	}break;
	default:
		break;
	}

	switch(nextIo)
	{
	case IO_OPERATION::IO_OPERATION_RECV_FROM_SOCKET:return true;
	case IO_OPERATION::IO_OPERATION_CLOSE_CONENCTION:
		{
			Shutdown();
			return true;
		}
	case IO_OPERATION::IO_OPERATION_NONE:
		{
			return false;
		}
	default:
		{
			//log error
			Shutdown();
			return true;
		}
	}
}

void CCommandSession::Shutdown()
{
	if(::shutdown(m_hSock,SD_SEND) == SOCKET_ERROR)
	{
		DWORD dwError = WSAGetLastError();
		//Log error;
	}
}

