#pragma once

#include "GenericSession.h"
#include <list>

class CSessionManager
{
public:
	CSessionManager(size_t sMaxSession = 1024);
	~CSessionManager();
	CGenericSession* CreateSession(SOCKET hSock);
	size_t DestroySession(CGenericSession* pSession);

	void DestroyDeadSessions();


private:
	size_t AddSession(CGenericSession* pSession);

	size_t RemoveSession(CGenericSession* pSession);

	size_t GetSessionCount()const;

private:

	std::list<CGenericSession*> m_sessionList;
	mutable CRITICAL_SECTION m_criticalSection;
	const size_t m_sMaxSession;

};
