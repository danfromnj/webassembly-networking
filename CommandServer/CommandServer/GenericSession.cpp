#pragma once
#include <WinSock2.h>
#include "GenericSession.h"

CGenericSession::CGenericSession():m_bDead(false)
{
	::ZeroMemory(&m_overlapped,sizeof(m_overlapped));
	m_overlapped.ioevent = IO_EVENT::IO_CONNECT;

	InitializeCriticalSection(&m_criticalSection);
}

CGenericSession::~CGenericSession()
{
	DeleteCriticalSection(&m_criticalSection);
};