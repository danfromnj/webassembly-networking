#pragma once
#include "SessionManager.h"
#include <vector>
#include "CommonDefine.h"

enum
{
	AS_SUCCESS = 0,

	AS_ERROR_UNKNOWN
};

enum
{
	SESSION_CONNECT_LEN		= -1,
	SESSION_CLOSE_LEN		= -2,
	SESSION_TIME_OUT_LEN	= -3,
	SYSTEM_SHUTDOWN_LEN		= -4
};


class CAysnServer
{
public:
	CAysnServer(unsigned short usDefPort,unsigned long uMaxCocurrentThread = 0);
	~CAysnServer();
	void StartServer();
	void StopServer();
private:
	WINDOWS_HANDLE m_hIocp;
	HANDLE m_hListenThread;
	unsigned short m_usDefPort;
	std::vector<HANDLE> m_vecWorkingThreads;
	WINDOWS_HANDLE m_hShutDownEvent;
	CSessionManager m_sessionMgr;
	unsigned long m_uMaxCocurrentThread;

private:
	static unsigned __stdcall SvrListenThread(void* param);
	unsigned DoListen();
	void TryServeNewConnection(SOCKET hSock);
	bool IsShutDown();

	static unsigned __stdcall SvrWorkingThread(void* param);
	unsigned DoWork();

	static unsigned __stdcall SessionTimeoutCheckThread(void* param);
	unsigned DoTimeoutCheck();

	unsigned long KickOffConnectEvent(CGenericSession* pSession);
};
