#pragma once

typedef BOOL (WINAPI *RELEASE)(UINT_PTR);
template <typename T, RELEASE ReleaseHandle = (RELEASE)&::CloseHandle, UINT_PTR InvalidHandle = NULL>
class auto_handle
{
public:
	auto_handle(T handle) : m_handle(handle) {}
	~auto_handle() { if(!Invalid()) (*ReleaseHandle)((UINT_PTR)m_handle); }
	operator T() { return m_handle; }
	bool Invalid() const { return m_handle == (T)InvalidHandle; }
private:
	T m_handle;
};

typedef auto_handle<SOCKET,(RELEASE)&::closesocket,INVALID_SOCKET> SOCKET_HANDLE;
typedef auto_handle<HANDLE,(RELEASE)&::CloseHandle,NULL> WINDOWS_HANDLE;