import datetime
import tornado.ioloop
import tornado.web
import tornado.websocket
import tornado.gen
from tornado.options import define, options
from tornado.concurrent import run_on_executor
from concurrent.futures import ThreadPoolExecutor

class EchoWebSocket(tornado.websocket.WebSocketHandler):
    def __init__(self, application, request, **kwargs):
        self.binary = False
        super(EchoWebSocket, self).__init__(application, request, **kwargs)

    def select_subprotocol(self, subprotocols):
        if "binary" in subprotocols:
            self.binary = True
            return "binary"
        return None
        
    def open(self):
        print("WebSocket opened binary mode = " + str(self.binary))
        self.write_message("Hello, I am a websocket server written in python.", binary=self.binary)

    def on_message(self, message):
        cmd = message.decode("utf-8") if self.binary else message
        if cmd.endswith("\r\n"):
            cmd = cmd[:len(cmd) - 2]
            if cmd.lower() == "date":
                datetime.datetime.now().strftime("%c")
                self.write_message(datetime.datetime.now().strftime("%c"), binary=self.binary)
            else:
                self.write_message(f"ERROR: unrecognized {cmd}.", binary=self.binary)
        else:
            self.write_message("ERROR: command must ends with \\r\\n.", binary=self.binary)

    def on_close(self):
        print("WebSocket closed")
        
    def check_origin(self, origin):
        return True

class EchoHandler(tornado.web.RequestHandler):
    def __init__(self, application, request, **kwargs):
        self.resonse_fmt = ''
        super(EchoHandler, self).__init__(application, request, **kwargs)
 
    def process_query(self, post=False):
        self.write("Hello")
        self.finish()


    def get(self):
        self.process_query(False)
    
    def post(self):
        self.process_query(True)

app = tornado.web.Application(handlers=[
    (r'/web/echo?', EchoHandler), 
    (r'/ws/echo?', EchoWebSocket),
    (r'/?', EchoWebSocket)])

define("port", default=8088, help="run on the given port", type=int)

if __name__ == "__main__":
    tornado.options.parse_command_line()
    app.listen(options.port)
    print ("WebSocket service is running at port %s" % options.port)
    tornado.ioloop.IOLoop.instance().start()
