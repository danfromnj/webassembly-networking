const worker = new Worker('./worker.js');

document.getElementById("btn_wsConnect").addEventListener("click", function() {
	let url = document.getElementById('ws_server_url').value;

	if(!(typeof self.ws === "undefined")){
		ws.close(1000, "close current connection.");
	}	
	ws = new WebSocket(url);
	ws.onopen = function(evt){
		console.log("websocket connected.");
	};

	ws.onclose = function(evt){
		console.log("websocket closed.");
	}

	ws.onmessage = function (evt) {
		if (document.getElementById('wrapInBlob').checked){
			let reader = new FileReader();
			reader.onload = function (e) {
				document.getElementById("txt_received").value = reader.result;
			}

			reader.readAsText(evt.data);
		}
		else{
			document.getElementById("txt_received").value = evt.data;
		}
	};
},
false);

document.getElementById("btn_wsSend").addEventListener("click", function() {
	let msg = document.getElementById("txt_to_send").value;
	if (document.getElementById('appendNewLine').checked)
	{
		msg += "\r\n";
	}

	ws.send(
		document.getElementById('wrapInBlob').checked ? 
		new Blob([msg], {type: 'text/plain'}) : msg
	);
},
false);

document.getElementById("btn_rawConnect").addEventListener("click", function() {
	const url = new URL(document.getElementById('ws_proxy_url').value);
	worker.postMessage({cmd: "connect", host:url.hostname, port:url.port});
},
false);