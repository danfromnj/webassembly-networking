// Following block works with modularize=0 wasm, which executes from main() function
var connected = false;
var Module={};
self.onmessage = function(e) {
    if(e.data.cmd === "connect"){
        if(!connected){
            Module["arguments"] = [e.data.host, e.data.port];
            self.importScripts("./multiplexing.js");
            console.log("connected.");
            connected = true;
        }
        else{
            console.log("already connected.")
        }
    }
}


// for modularized wasm 
// self.importScripts("./multiplexing.js");

// self.Module().then(M=>{
//     console.log("web assembly loaded.");
//     self.Module = M;
//     self.makeConnection = self.Module.cwrap('MakeConnection','number', ['number', 'number']);
//     const buf = str2ptr(self.Module, "127.0.0.1");
//     self.makeConnection(buf,17904);
// });


function str2ptr(Module,s){
    const ptr = Module._malloc((s.length+1)*Uint8Array.BYTES_PER_ELEMENT);
    for (let i = 0; i < s.length; i++) {
            Module.setValue(ptr+i, s.charCodeAt(i), 'i8');
          }
    Module.setValue(ptr+s.length, 0, 'i8');
    return ptr;
}